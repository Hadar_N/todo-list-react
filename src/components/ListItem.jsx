import styled from 'styled-components';

const ListItem = ({ name, isCompleted, changeCompleted, deleteSelf }) => {

    return (
    <Item isCompleted={isCompleted}>
        <input type="checkbox" checked={isCompleted} onChange={changeCompleted} />
        <span>{name}</span>
        <button className="redX" onClick={deleteSelf}>X</button>
    </Item>
    )
}

export default ListItem;

const Item = styled.div`
    width:100%;
    font-size: 1.5rem;
    position: relative;
    padding-bottom: 0.3rem;

    input {
        margin-right: 1rem;
        height: 1.5rem;
        width: 1.5rem;
        position:relative;
        top: 4px;

        :focus {outline:0;}

    }

    span{
        text-decoration: ${props => props.isCompleted ? "line-through": "none"};
        color: ${props => props.isCompleted ? "gray": "black"};
    }

    .redX {
        display:none;
        position: absolute;
        right: 1rem;
        top: 0.5rem;

        background: none;
        cursor: pointer;
        border: 2px solid red;
        border-radius: 50%;
        width:1.4rem;
        height: 1.4rem;

        font-size: .7rem;
        color: red;

        :focus {outline:0;}

    }

    :hover {
        .redX {
            display: inline;
        }
    }
`;