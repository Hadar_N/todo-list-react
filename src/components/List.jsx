import { useState, useEffect } from "react";
import ListItem from "./ListItem"

const List = ({ newListItem, added, submitForm, addToNum, viewType, clearForm, formCleared }) => {
    const [listItems, setListItems] = useState([]);

    //clear form
    useEffect(() => {
        setListItems(listItems.filter(item => !item.isCompleted));
        formCleared();
    }, [clearForm]);

    //add new item (submitForm)
    useEffect(() => {
        if (newListItem) {    
            setListItems([...listItems, { name: newListItem, isCompleted: false }]);
            added();
            addToNum(1);
        }

    }, [submitForm]);


    const changeCompleted = (i) => {
        return () => {
            const newList = [...listItems];
            newList[i].isCompleted = !newList[i].isCompleted;
            setListItems([...newList]);

            if (newList[i].isCompleted) addToNum(-1)
            else addToNum(1);
        }
    }

    const deleteItem = (i) => {
        return () => { if (!listItems[i].isCompleted) addToNum(-1); setListItems([...listItems.slice(0, i), ...listItems.slice(i + 1, listItems.length)]) }
    }

    const checkToShow = (isComp) => {
        if (viewType === "All" || (viewType === "Completed" && isComp) || (viewType === "Active" && !isComp))
            return true;
        else return false;
    }

    return <ul style={{ listStyle: "none", width: "100%", padding: 0 }}>
        {listItems.map((item, i) =>
            <li key={i}> {checkToShow(item.isCompleted) ?
                <ListItem {...item} changeCompleted={changeCompleted(i)} deleteSelf={deleteItem(i)} />
                :
                null}
            </li>
        )}</ul>
}

export default List;
