import { useState } from 'react';
import List from './components/List';
import styled from 'styled-components';

function App() {
  const [newTaskName, setNewTaskName] = useState("");
  const [submitForm, setSubmitForm] = useState(false);
  const [numOfItems, setNumOfItems] = useState(0);
  const [viewType,setViewType] = useState("All");
  const [clearForm, setClearForm] = useState(false);

  const addTask = (e) => {
    e.preventDefault();
    setSubmitForm(true);
    // console.log(newTaskName);
  }
  const taskAdded = () => {
    setNewTaskName("");
    setSubmitForm(false);
  }

  const checkClass = (txt) => {
    if (txt === viewType) return "current";
    else return "";
  }

  return (
    <AppClass>
      <AppInner>
      <Form>
      <input placeholder="new task" value={newTaskName} onChange={(e) => setNewTaskName(e.target.value)}></input>
      <button onClick={addTask}>Add</button>
      </Form>
      <List newListItem = {newTaskName} added={taskAdded} submitForm={submitForm} addToNum={(num) => setNumOfItems(numOfItems + num)} viewType={viewType} clearForm={clearForm} formCleared={() => setClearForm(false)}></List>
      <Footer>
        <span>{numOfItems} items left</span>
        <section className="midButtons">
          <button className={checkClass("All")} onClick={(e) => setViewType(e.target.innerHTML)}>All</button>
          <button className={checkClass("Active")} onClick={(e) => setViewType(e.target.innerHTML)}>Active</button>
          <button className={checkClass("Completed")} onClick={(e) => setViewType(e.target.innerHTML)}>Completed</button>
        </section>
        <button onClick={() => setClearForm(true)}>clear completed</button>
      </Footer>
      </AppInner>
    </AppClass>
  );
}

export default App;

const AppClass= styled.div`
display:flex;
justify-content:center;
align-items: flex-start;
padding: 3rem;
background-color: rgba(234,201,256,0.3);
min-height: calc(100vh - 6rem);
`;
const AppInner= styled.div`
display:flex;
flex-direction:column;
align-items:center;
width:35rem;
background-color:white;
`;

const Form = styled.form`
  width: 100%;
  font-size: 1.5rem;
  display: flex;

  input {
    flex:3;
    font:inherit;
    padding: 5px;
  }
  button {
    width:5rem;
    font:inherit;
    font-size: 1.2rem;
  }
`;

const Footer = styled.footer`
  width: calc(100% - 20px);
  display:flex;
  justify-content: space-between;
  margin: 7px;
  font-size: 0.7rem;

  button {
    font:inherit;
    background: none;
    border: none;
    cursor: pointer;
    border-radius:4px;
    
    :focus {outline:0;}

    :hover {
      text-decoration: underline;
    }
  }

  .midButtons {
    
    button {
      margin:2px;
      border: 1px solid transparent;

      :hover {
        text-decoration: none;

        :not(.current) {
        border: 1px solid red;
        }
      }
    }
    .current {
      border: 1px solid blue;
    }
  }

`;
